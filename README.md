# RC3 map for assembly "Spielekiste"
[Speielkiste Assembly](https://signup.c3assemblies.de/assembly/f9426f3a-287b-48bd-bdf6-653f305c4b8b)
[Jitsi Meet](https://meet.ffmuc.net/Weltenwanderer)

## Tasks
 * connect all the maps
 * [testing, ](https://play.wa-test.rc3.cccv.de/_/global/raw.githubusercontent.com/Weltenwanderer/WeltiConRoom/master/main.json)
 [testing, testing!](https://play.wa-test.rc3.cccv.de/_/global/raw.githubusercontent.com/Weltenwanderer/WeltiConRoom/master/Labyrinth/Labyrinth_Entry.json)
 * make sure maps are in __json__ format
 * make sure maps are saved in finite size
 * make sure all maps load common ressources from the same location
 * include magic the gathering online
 * specialized gaming room for party games
 * add interactive text

## Goals
 * Guide the visitors in a way, that they can empower themselves. There should be as little supervision as possible.
  * more tutorials
  * required hardware, required steps
  * how to use smartphone as webcam
 * Networking: Find out how other players have digitized their analog gaming.
 * Education: Show how we roll digitally. Help stragers learn new games.
 * Find ways to circumvent ticket shortage
 
 
